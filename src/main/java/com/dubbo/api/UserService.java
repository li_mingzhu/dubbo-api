package com.dubbo.api;

/**
 * Created by YYT on 2017/7/3.
 */
public interface UserService {
    String sayHi(String name);
}
